Source: s6
Section: admin
Priority: optional
Maintainer: Shengjing Zhu <zhsj@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
Build-Depends-Arch:
 libexecline-dev (>= 2.9.0.0),
 skalibs-dev (>= 2.14.3.0),
Standards-Version: 4.7.0
Homepage: https://skarnet.org/software/s6/
Vcs-Git: https://salsa.debian.org/zhsj/s6.git
Vcs-Browser: https://salsa.debian.org/zhsj/s6

Package: libs6-2.13
Architecture: any
Section: libs
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 s6 (<< 2.8.0.1),
Description: small and secure supervision software suite (shared library)
 s6 is a small suite of programs for UNIX, designed to allow process
 supervision (a.k.a service supervision), in the line of daemontools and
 runit, as well as various operations on processes and daemons. It is meant
 to be a toolbox for low-level process and service administration, providing
 different sets of independent tools that can be used within or without the
 framework, and that can be assembled together to achieve powerful
 functionality with a very small amount of code.
 .
 This package contains shared libraries.

Package: libs6-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 libs6-2.13 (= ${binary:Version}),
 ${misc:Depends},
Description: small and secure supervision software suite (development files)
 s6 is a small suite of programs for UNIX, designed to allow process
 supervision (a.k.a service supervision), in the line of daemontools and
 runit, as well as various operations on processes and daemons. It is meant
 to be a toolbox for low-level process and service administration, providing
 different sets of independent tools that can be used within or without the
 framework, and that can be assembled together to achieve powerful
 functionality with a very small amount of code.
 .
 This package contains static and header files.

Package: s6
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 libs6-2.7,
Replaces:
 libs6-2.7,
Recommends:
 execline,
Suggests:
 s6-doc,
Description: small and secure supervision software suite
 s6 is a small suite of programs for UNIX, designed to allow process
 supervision (a.k.a service supervision), in the line of daemontools and
 runit, as well as various operations on processes and daemons. It is meant
 to be a toolbox for low-level process and service administration, providing
 different sets of independent tools that can be used within or without the
 framework, and that can be assembled together to achieve powerful
 functionality with a very small amount of code.
 .
 Documentation is provided in s6-doc package.

Package: s6-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: small and secure supervision software suite (documentation)
 s6 is a small suite of programs for UNIX, designed to allow process
 supervision (a.k.a service supervision), in the line of daemontools and
 runit, as well as various operations on processes and daemons. It is meant
 to be a toolbox for low-level process and service administration, providing
 different sets of independent tools that can be used within or without the
 framework, and that can be assembled together to achieve powerful
 functionality with a very small amount of code.
 .
 This package contains documentation files.
